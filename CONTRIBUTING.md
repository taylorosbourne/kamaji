# Contributing 🎉

Thanks for wanting to help out, you rock! 😎 Here's how you can get started:

```shell
git clone https://gitlab.com/taylorosbourne/kamaji.git
cd kamaji
```

## Additional ways to contribute:

- 🐛 Report a bug [here](https://gitlab.com/taylorosbourne/kamaji/-/blob/main/.gitlab/issue_templates/bug.md)
