require "ecr/macros"
require "option_parser"
require "dir"
require "colorize"
require "../utils/git"
require "../utils/messages"
require "../utils/generator"

module Kamaji
  class Build
    class_property valid_languages = [
      "assemblyscript",
      "bash",
      "crystal",
      "deno",
      "elixir",
      "fish",
      "go",
      "kotlin",
      "node",
      "ruby",
      "rust",
      "scala",
    ]

    class_property valid_types = [
      "app",
      "cli",
      "lib",
      "script",
      "wasm",
    ]

    class Error
      def self.new(message)
        puts "#{message}\n"
        exit
      end
    end

    def run(args)
      config = parse_args(args)
      Generator.new(config).build
      Messages::PostRun.new(config.lang).run
    end

    def parse_args(args)
      config = Config.new
      OptionParser.parse(args) do |parser|
        parser.banner = <<-USAGE
        \nUsage: build #{"LANG".colorize(:green)} #{"TYPE".colorize(:cyan)} NAME

          Initializes a project folder as a git repository and
          default folder structure for programming projects.

        USAGE

        parser.on("-h", "--help", "show this help") do
          puts parser
          Messages.build_help(Build.valid_languages, Build.valid_types)
          exit
        end

        parser.unknown_args do |u_args|
          config.lang = fetch_arg(parser, u_args, "LANG", Build.valid_languages)
          config.type = fetch_arg(parser, u_args, "TYPE", Build.valid_types)
          name = fetch_required_parameter(parser, u_args, "NAME")
          config.name = name
          config.dir = name
        end
      end

      config.author = fetch_git_info("user.name", "your name")
      config.email = fetch_git_info("user.email", "youremail@example.com")
      config.gitlab_name = fetch_git_info("gitlab.user", "your username")
      config.description = "Creates a new project"

      validate_name(config.name)

      config
    end

    def fetch_git_info(param, default)
      Git.git_config(param) || default
    end

    def fetch_arg(opts, args, name, valid_options)
      capitalized = name.split("").map { |x| x.capitalize }.join("")
      arg = fetch_required_parameter(opts, args, capitalized)
      unless arg.downcase.in?(valid_options)
        raise Error.new "\n Invalid #{capitalized} value: #{arg}"
      end
      arg.downcase
    end

    def fetch_required_parameter(opts, args, name)
      if args.empty?
        raise Error.new Messages.argument_missing(name)
      end
      args.shift
    end

    def validate_name(name)
      error_prefix = "\n #{"Error:".colorize(:red)} #{"NAME".colorize(:yellow)}"
      case
      when name.blank?                       then raise Error.new("#{error_prefix} must not be empty")
      when name.size > 50                    then raise Error.new("#{error_prefix} must not be longer than 50 characters")
      when name.each_char.any?(&.uppercase?) then raise Error.new("#{error_prefix} should be all lower cased")
      when !name[0].ascii_letter?            then raise Error.new("#{error_prefix} must start with a letter")
      when name.index("--")                  then raise Error.new("#{error_prefix} must not have consecutive dashes")
      when name.index("__")                  then raise Error.new("#{error_prefix} must not have consecutive underscores")
      when !name.each_char.all? { |c| c.alphanumeric? || c == '-' || c == '_' }
        raise Error.new("NAME must only contain alphanumerical characters, underscores or dashes")
      else
        # name is valid
      end
    end

    class Config
      property name : String
      property type : String
      property lang : String
      property dir : String
      property author : String
      property email : String
      property gitlab_name : String
      property description : String

      def initialize(
        @name = "none",
        @lang = "none",
        @dir = "none",
        @author = "none",
        @email = "none",
        @gitlab_name = "none",
        @description = "none",
        @type = "app"
      )
      end

      getter expanded_dir : ::Path { ::Path.new(dir).expand(home: true) }

      getter gitlab_repo : String { "#{gitlab_name}/#{expanded_dir.basename}" }
    end
  end
end
