require "option_parser"
require "./command/build"

module Kamaji
  class Command
    VERSION = "v0.0.1"

    USAGE = <<-USAGE
        Usage: build #{"LANG".colorize(:green)} #{"TYPE".colorize(:cyan)} NAME

          Initializes a project folder as a git repository and
          default folder structure for programming projects.

          Run #{"kamaji build -h".colorize(:green)} to view more information
          about supported #{"LANG".colorize(:green)} and #{"TYPE".colorize(:cyan)} options.
        USAGE

    def self.run(options = ARGV)
      new(options).run
    end

    private getter options

    def initialize(@options : Array(String))
    end

    def run
      command = options.first?
      case
      when !command
        puts usage_with_logo
        exit
      when "build".starts_with?(command)
        options.shift
        build
      when "help".starts_with?(command), "--help" == command, "-h" == command
        puts usage_with_logo
        exit
      when "version".starts_with?(command), "--version" == command, "-v" == command
        puts "\nKamaji CLI Version: #{VERSION.colorize(:green)}"
        exit
      else
        error "unknown command: #{command}"
      end
    end

    def usage_with_logo
      [
        "\n",
        "    __ __                        _ _    _.---.                       .---.    ",
        "   / //_/___ _____ ___  ____ _  (_|_)  '---,  `.___________________.'  _  `.  ",
        "  / ,< / __ `/ __ `__ \\/ __ `/ / / /        )   ___________________   <_>  : ",
        " / /| / /_/ / / / / / / /_/ / / / /    .---'  .'                   `.     .'  ",
        "/_/ |_\\__,_/_/ /_/ /_/\\__,_/_/ /_/      `----'                       `---'  ",
        "                          /___/                                               ",
      ].map { |line| puts line.colorize(:green) }
      puts "\n#{USAGE}"
    end

    private def build
      Build.new.run(options)
    end

    private def error(msg, exit_code = 1)
      Build::Error.new(msg)
    end
  end

  Command.run
end
