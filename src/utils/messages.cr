module Kamaji::Messages
  def self.no_template_exists(type, lang)
    [
      "\n Darn it!  We don't have a #{type.colorize(:green)} template for #{lang.colorize(:cyan)}. #{"¯\\_(ツ)_/¯".colorize(:red)}",
      "\n If you want to support this project and add the missing template, please consider contributing!",
      "\n https://gitlab.com/taylorosbourne/kamaji/-/blob/main/CONTRIBUTING.md".colorize(:yellow),
    ].map { |line| puts line }
  end

  def self.build_help(valid_langs, valid_types)
    puts "\n  #{"LANG".colorize(:green)} Options"
    valid_langs.each do |lang|
      puts "    - #{lang}"
    end
    puts "\n  #{"TYPE".colorize(:cyan)} Options"
    valid_types.each do |type|
      puts "    - #{type}"
    end
    [
      "\n  NOTE".colorize(:red),
      "\n    Not all #{"TYPE".colorize(:cyan)} options are valid with all #{"LANG".colorize(:green)} options.",
      "\n    Read about valid options here #{"https://gitlab.com/taylorosbourne/kamaji".colorize(:yellow)}",
    ].map { |line| puts line }
  end

  def self.argument_missing(name)
    empty_string = ""
    [
      "\n Womp womp, argument #{name.colorize(:red)} is #{"missing".colorize(:yellow)}",
      "\n Kamaji requires three parameters: #{"LANG".colorize(:green)}, #{"TYPE".colorize(:cyan)}, #{"NAME"}",
      "\n You can read more about arguments here: #{"https://gitlab.com/taylorosbourne/kamaji".colorize(:yellow)}",
    ].map { |line| puts line }
    empty_string
  end

  class PostRun
    private getter lang

    def initialize(@lang = "")
    end

    def run
      case
      when lang == "assemblyscript" then assemblyscript
      when lang == "bash"           then bash
      when lang == "crystal"        then crystal
      when lang == "deno"           then deno
      when lang == "elixir"         then elixir
      when lang == "fish"           then fish
      when lang == "go"             then go
      when lang == "kotlin"         then kotlin
      when lang == "node"           then node
      when lang == "ruby"           then ruby
      when lang == "rust"           then rust
      when lang == "scala"          then scala
      else
      end
    end

    def assemblyscript
      [
        "\n                   ",
        "         /\\         ",
        "        /  \\        ",
        "       /    \\       ",
        "      /______\\      ",
        "     |        |      ",
        "     |        |      #{"    ___                             __    __      _____           _       __       ".colorize(:green)}",
        "     |        |      #{"   /   |  _____________  ____ ___  / /_  / /_  __/ ___/__________(_)___  / /_      ".colorize(:green)}",
        "     |        |      #{"  / /| | / ___/ ___/ _ \\/ __ `__ \\/ __ \\/ / / / /\\__ \\/ ___/ ___/ / __ \\/ __/".colorize(:green)}",
        "     |        |      #{" / ___ |(__  |__  )  __/ / / / / / /_/ / / /_/ /___/ / /__/ /  / / /_/ / /_        ".colorize(:green)}",
        "     |        |      #{"/_/  |_/____/____/\\___/_/ /_/ /_/_.___/_/\\__, //____/\\___/_/  /_/ .___/\\__/    ".colorize(:green)}",
        "    /|   ||   |\\    #{"                                         /____/                 /_/                ".colorize(:green)}",
        "   / |   ||   | \\   ",
        "  /  |   ||   |  \\  ",
        " /___|   ||   |___\\ ",
        "     |        |      ",
        "      \\      /      ",
        "       ||  ||        ",
      ].map { |line| puts line.colorize(:light_cyan) }

      puts "\n Your new #{"AssemblyScript".colorize(:light_cyan)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} yarn test   #{"# Runs tests".colorize(:yellow)}"
      puts "    └── #{">".colorize(:cyan)} yarn asbuild #{"# Builds code for production".colorize(:yellow)}"
    end

    def bash
      [
        "\n              ",
        "        **      ",
        "   ************ ",
        "   **   **      ",
        "   **   **           #{" ______     ______     ______     __  __                    ".colorize(:green)}",
        "   **   **           #{"/\\  == \\   /\\  __ \\   /\\  ___\\   /\\ \\_\\ \\         ".colorize(:green)}",
        "   ************      #{"\\ \\  __<   \\ \\  __ \\  \\ \\___  \\  \\ \\  __ \\       ".colorize(:green)}",
        "        **   **      #{" \\ \\_____\\  \\ \\_\\ \\_\\  \\/\\_____\\  \\ \\_\\ \\_\\ ".colorize(:green)}",
        "        **   **      #{"  \\/_____/   \\/_/\\/_/   \\/_____/   \\/_/\\/_/           ".colorize(:green)}",
        "        **   ** ",
        "   ************ ",
        "        **      ",
      ].map { |line| puts line.colorize(:magenta) }

      puts "\n Your new #{"Bash".colorize(:magenta)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    └── #{">".colorize(:cyan)} bash your_project.sh #{"# Run your".colorize(:yellow)} #{"bash".colorize(:magenta)} #{"script".colorize(:yellow)}"
    end

    def crystal
      [
        "\n                             ",
        "              v                ",
        "       \\             /        ",
        "                               ",
        "          _________            ",
        " `       /         \\        ' ",
        "        /  \\     /  \\            #{"      ______     ______     __  __     ______     ______   ______     __                                ".colorize(:green)}",
        "       /             \\            #{"    /\\  ___\\   /\\  == \\   /\\ \\_\\ \\   /\\  ___\\   /\\__  _\\ /\\  __ \\   /\\ \\                ".colorize(:green)}",
        "_     /   _ _ . _ _   \\      _    #{"    \\ \\ \\____  \\ \\  __<   \\ \\____ \\  \\ \\___  \\  \\/_/\\ \\/ \\ \\  __ \\  \\ \\ \\____       ".colorize(:green)}",
        "      \\               /           #{"     \\ \\_____\\  \\ \\_\\ \\_\\  \\/\\_____\\  \\/\\_____\\    \\ \\_\\  \\ \\_\\ \\_\\  \\ \\_____\\ ".colorize(:green)}",
        "       \\             /            #{"      \\/_____/   \\/_/ /_/   \\/_____/   \\/_____/     \\/_/   \\/_/\\/_/   \\/_____/                  ".colorize(:green)}",
        "        \\  /     \\  /        ",
        " '       \\_________/        ` ",
        "                               ",
        "                               ",
        "       /             \\        ",
        "              ^                ",
      ].map { |line| puts line.colorize(:white) }

      puts "\n Your new #{"Crystal".colorize(:white)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} shards install      #{"# Installs project deps".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} crystal tool format #{"# Runs".colorize(:yellow)} #{"crystal's".colorize(:white)} #{"built in formatter".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} crystal spec        #{"# Runs tests located in spec directory".colorize(:yellow)}"
      puts "    └── #{">".colorize(:cyan)} crystal build       #{"# Runs".colorize(:yellow)} #{"crystal's".colorize(:white)} #{"compiler to build new executable".colorize(:yellow)}"
    end

    def deno
      [
        "\n                 ",
        "               __  ",
        "              / _)     #{" _____     ______     __   __    ______                     ".colorize(:green)}",
        "     _.----._/ /       #{"/\\  __-.  /\\  ___\\   /\\ \"-.\\ \\  /\\  __ \\           ".colorize(:green)}",
        "    /         /        #{"\\ \\ \\/\\ \\ \\ \\  __\\   \\ \\ \\-.  \\ \\ \\ \\/\\ \\  ".colorize(:green)}",
        " __/ (  | (  |         #{" \\ \\____-  \\ \\_____\\  \\ \\_\\\\\"\\_\\ \\ \\_____\\   ".colorize(:green)}",
        "/__.-'|_|--|_|         #{"  \\/____/   \\/_____/   \\/_/ \\/_/  \\/_____/             ".colorize(:green)}",
      ].map { |line| puts line.colorize(:cyan) }

      puts "\n Your new #{"Deno".colorize(:cyan)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    └── #{">".colorize(:cyan)} deno run mod.ts #{"# Run your".colorize(:yellow)} #{"deno".colorize(:cyan)} #{"app".colorize(:yellow)}"
    end

    def elixir
      [
        "\n             ",
        "      _____    ",
        "     `.___,'   ",
        "      (___)    ",
        "      <   >    ",
        "       ) (     ",
        "      /`-.\\       #{" ______     __         __     __  __     __     ______                         ".colorize(:green)}",
        "     /     \\      #{"/\\  ___\\   /\\ \\       /\\ \\   /\\_\\_\\_\\   /\\ \\   /\\  == \\          ".colorize(:green)}",
        "    / _    _\\     #{"\\ \\  __\\   \\ \\ \\____  \\ \\ \\  \\/_/\\_\\/_  \\ \\ \\  \\ \\  __<       ".colorize(:green)}",
        "   :,' `-.' `:     #{"\\ \\_____\\  \\ \\_____\\  \\ \\_\\   /\\_\\/\\_\\  \\ \\_\\  \\ \\_\\ \\_\\  ".colorize(:green)}",
        "   |         |     #{" \\/_____/   \\/_____/   \\/_/   \\/_/\\/_/   \\/_/   \\/_/ /_/                ".colorize(:green)}",
        "   :         ; ",
        "    \\       / ",
        "     `.___.'   ",
      ].map { |line| puts line.colorize(:light_magenta) }

      puts "\n Your new #{"Elixir".colorize(:light_magenta)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} mix run     #{"# Runs a particular command inside the project".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} mix test    #{"# Runs tests for the given project".colorize(:yellow)}"
      puts "    └── #{">".colorize(:cyan)} mix compile #{"# Compiles the current project".colorize(:yellow)}"
    end

    def fish
      [
        "\n                                         ",
        "                     ___                   ",
        "      ___======____=---=)                  ",
        "   /T            \\_--===)                 ",
        "   [ \\ (O)   \\~    \\_-==)               ",
        "   \\      / )J~~    \\-=)              #{" ______   __     ______     __  __                     ".colorize(:green)}",
        "   \\\\___/  )JJ~~~   \\)               #{" /\\  ___\\ /\\ \\   /\\  ___\\   /\\ \\_\\ \\         ".colorize(:green)}",
        "    \\_____/JJJ~~~~    \\               #{"\\ \\  __\\ \\ \\ \\  \\ \\___  \\  \\ \\  __ \\       ".colorize(:green)}",
        "    / \\  , \\J~~~~~     \\             #{"  \\ \\_\\    \\ \\_\\  \\/\\_____\\  \\ \\_\\ \\_\\   ".colorize(:green)}",
        "   (-\\)\\=|\\\\\\~~~~       L__        #{"     \\/_/     \\/_/   \\/_____/   \\/_/\\/_/          ".colorize(:green)}",
        "   (\\\\)  (\\\\\\)_           \\==__      ",
        "    \\V    \\\\\\) ===_____   \\\\\\\\\\\\ ",
        "           \\V)     \\_) \\\\\\\\JJ\\J\\)  ",
        "                       /J\\JT\\JJJJ)       ",
        "                       (JJJ| \\UUU)        ",
        "                        (UU)               ",
      ].map { |line| puts line.colorize(:red) }

      puts "\n Your new #{"Fish".colorize(:red)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    └── #{">".colorize(:cyan)} fish your_project.fish #{"# Run your".colorize(:yellow)} #{"fish".colorize(:red)} #{"script".colorize(:yellow)}"
    end

    def go
      [
        "\n                                   ",
        "         ,_---~~~~~----._            ",
        "   _,,_,*^____      _____``*g*\\\"*, ",
        "  / __/ /'     ^.  /      \\ ^@q   f ",
        " [  @f | @))    |  | @))   l  0 _/   #{" ______     ______                 ".colorize(:green)}",
        "  \\`/   \\~____ / __ \\_____/    \\ #{"    /\\  ___\\   /\\  __ \\        ".colorize(:green)}",
        "   |           _l__l_           I    #{"\\ \\ \\__ \\  \\ \\ \\/\\ \\      ".colorize(:green)}",
        "   }          [______]           I   #{" \\ \\_____\\  \\ \\_____\\        ".colorize(:green)}",
        "   ]            | | |            |   #{"  \\/_____/   \\/_____/            ".colorize(:green)}",
        "   ]             ~ ~             |   ",
        "   |                            |    ",
        "    |                           |    ",
      ].map { |line| puts line.colorize(:cyan) }

      puts "\n Your new #{"Go".colorize(:cyan)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} go run   #{"# Runs your code".colorize(:yellow)}"
      puts "    └── #{">".colorize(:cyan)} go build #{"# Builds your code".colorize(:yellow)}"
    end

    def kotlin
      [
        "\n                   ",
        " ___________________ ",
        "|      /           / ",
        "|     /           /  ",
        "|    /           /   ",
        "|   /           /    ",
        "|  /           /     #{"    __ ______  ________    _____   __  ".colorize(:cyan)}",
        "| /           /      #{"   / //_/ __ \\/_  __/ /   /  _/ | / / ".colorize(:cyan)}",
        "|/           /       #{"  / ,< / / / / / / / /    / //  |/ /   ".colorize(:cyan)}",
        "|           /\\      #{"  / /| / /_/ / / / / /____/ // /|  /   ".colorize(:cyan)}",
        "|          /  \\     #{" /_/ |_\\____/ /_/ /_____/___/_/ |_/   ".colorize(:cyan)}",
        "|         /    \\    ",
        "|        /      \\   ",
        "|       /        \\  ",
        "|      /          \\ ",
        "|_____/____________\\",
      ].map { |line| puts line.colorize(:yellow) }

      puts "\n Your new #{"Kotlin".colorize(:light_cyan)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} gradle build"
      puts "    └── #{">".colorize(:cyan)} gradle wrapper"
    end

    def node
      [
        "\n              ",
        "          ,/    ",
        "        ,'/     ",
        "      ,' /       #{" __   __     ______     _____     ______                   ".colorize(:green)}",
        "    ,'  /_____,  #{"/\\ \"-.\\ \\   /\\  __ \\   /\\  __-.  /\\  ___\\         ".colorize(:green)}",
        "  .'____    ,'   #{"\\ \\ \\-.  \\  \\ \\ \\/\\ \\  \\ \\ \\/\\ \\ \\ \\  __\\ ".colorize(:green)}",
        "       /  ,'     #{" \\ \\_\\\\\"\\_\\  \\ \\_____\\  \\ \\____-  \\ \\_____\\ ".colorize(:green)}",
        "      / ,'       #{"  \\/_/ \\/_/   \\/_____/   \\/____/   \\/_____/           ".colorize(:green)}",
        "     /,'        ",
        "    /'          ",
      ].map { |line| puts line.colorize(:light_cyan) }

      puts "\n Your new #{"Node".colorize(:light_cyan)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} yarn dev   #{"# Runs dev server".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} yarn start #{"# Runs production server".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} yarn build #{"# Builds code for production".colorize(:yellow)}"
      puts "    └── #{">".colorize(:cyan)} yarn test  #{"# Runs your tests".colorize(:yellow)}"
    end

    def ruby
      [
        "\n                                                                                                                     ",
        "        __________________                                                                                             ",
        "      .-'  \\ _.-''-._ /  '-.                                                                                          ",
        "    .-/\\   .'.      .'.   /\\-.                                                                                       ",
        "   _'/  \\.'   '.  .'   './  \\'_   #{"      ______     __  __    ______     __  __                 ".colorize(:green)}",
        "  :======:======::======:======:    #{"   /\\  == \\   /\\ \\/\\ \\  /\\  == \\   /\\ \\_\\ \\      ".colorize(:green)}",
        "   '. '.  \\     ''     /  .' .'    #{"    \\ \\  __<   \\ \\ \\_\\ \\ \\ \\  __<   \\ \\____ \\    ".colorize(:green)}",
        "     '. .  \\   :  :   /  . .'      #{"     \\ \\_\\ \\_\\  \\ \\_____\\ \\ \\_____\\  \\/\\_____\\ ".colorize(:green)}",
        "       '.'  \\  '  '  /  '.'        #{"      \\/_/ /_/   \\/_____/  \\/_____/   \\/_____/           ".colorize(:green)}",
        "         ':  \\:    :/  :'                                                                                             ",
        "           '. \\    / .'                                                                                               ",
        "             '.\\  /.'                                                                                                 ",
        "               '\\/'                                                                                                   ",
      ].map { |line| puts line.colorize(:red) }

      puts "\n Your new #{"Ruby".colorize(:red)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} rubocop --require rubocop-airbnb #{"# Runs".colorize(:yellow)} #{"rubocop".colorize(:red)} #{"for linting".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} bundle install                   #{"# Installs project deps".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} srb init                         #{"# Initializes #{"Sorbet".colorize(:magenta)}".colorize(:yellow)}"
      puts "    └── #{">".colorize(:cyan)} srb tc                           #{"# Runs".colorize(:yellow)} #{"Sorbet's".colorize(:magenta)} #{"type-checker".colorize(:yellow)}"
    end

    def rust
      [
        "\n                           ",
        "        ___     ___          ",
        "      .i .-'   `-. i.        ",
        "    .'   `/     \\'  _`.     ",
        "    |,-../ o   o \\.' `|     ",
        " (| |   /  _\\ /_  \\   | |)  #{"   ______     __  __     ______     ______                 ".colorize(:green)}",
        "  \\\\  (_.'.'\"`.`._)  ///  #{"    /\\  == \\   /\\ \\/\\ \\   /\\  ___\\   /\\__  _\\     ".colorize(:green)}",
        "   \\`._(..:   :..)_.'//     #{"  \\ \\  __<   \\ \\ \\_\\ \\  \\ \\___  \\  \\/_/\\ \\/    ".colorize(:green)}",
        "    \\`.__\\ .:-:. /__.'/     #{"   \\ \\_\\ \\_\\  \\ \\_____\\  \\/\\_____\\    \\ \\_\\  ".colorize(:green)}",
        "     `-i-->.___.<--i-'        #{"  \\/_/ /_/   \\/_____/   \\/_____/     \\/_/              ".colorize(:green)}",
        "     .'.-'/.=^=.\\`-.`.      ",
        "    /.'  //     \\\\  `.\\   ",
        "   ||   ||       ||   ||     ",
        "   \\)   ||       ||   (/    ",
        "        \\)       (/         ",
      ].map { |line| puts line.colorize(:yellow) }

      puts "\n Your new #{"Rust".colorize(:yellow)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    ├── #{">".colorize(:cyan)} cargo run   #{"Run a binary or example of your code".colorize(:yellow)}"
      puts "    ├── #{">".colorize(:cyan)} cargo build #{"Compiles your code".colorize(:yellow)}"
      puts "    └── #{">".colorize(:cyan)} cargo test  #{"Runs your tests".colorize(:yellow)}"
    end

    def scala
      [
        "\n                     ",
        "     _____________     ",
        "    /            /     ",
        "   /____________/       #{" ______     ______     ______     __         ______                       ".colorize(:green)}",
        "    _____________       #{"/\\  ___\\   /\\  ___\\   /\\  __ \\   /\\ \\       /\\  __ \\            ".colorize(:green)}",
        "   /            /       #{"\\ \\___  \\  \\ \\ \\____  \\ \\  __ \\  \\ \\ \\____  \\ \\  __ \\      ".colorize(:green)}",
        "  /____________/        #{" \\/\\_____\\  \\ \\_____\\  \\ \\_\\ \\_\\  \\ \\_____\\  \\ \\_\\ \\_\\ ".colorize(:green)}",
        "   _____________        #{"  \\/_____/   \\/_____/   \\/_/\\/_/   \\/_____/   \\/_/\\/_/             ".colorize(:green)}",
        "  /            /       ",
        " /____________/        ",
      ].map { |line| puts line.colorize(:light_red) }

      puts "\n Your new #{"Scala".colorize(:light_red)} project is ready!"
      puts "  #{"Useful Commands".colorize(:green)}:"
      puts "    └── #{">".colorize(:cyan)} sbt run #{"# Run your".colorize(:yellow)} #{"scala".colorize(:light_red)} #{"app".colorize(:yellow)}"
    end
  end
end
