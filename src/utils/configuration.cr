require "colorize"

class Configuration
  class_property config_path = "#{Path.home}/.config/kamaji.yml"

  def self.read_config(config)
    if File.exists?(config_path)
      puts "Using config found at #{config_path}"
      # return => custom_config
      config
    else
      [
        "\n No configuration file found.  Using default configuration.",
        "\n You can learn more about configuring kamaji here",
        "\n https://gitlab.com/taylorosbourne/kamaji".colorize(:yellow),
        "\n",
      ].map { |line| puts line }
      config
    end
  end
end
