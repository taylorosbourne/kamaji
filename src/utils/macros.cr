require "../templates/project"

macro template(name, template_path, destination_path, lang)

  class {{name.id}} < Project::View

    ECR.def_to_s {{ "#{lang.id}/#{template_path.id}" }}

    def path
      {{destination_path}}
    end
  end
end
