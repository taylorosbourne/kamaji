require "./configuration"
require "../templates/project"
require "../templates/assemblyscript"
require "../templates/bash"
require "../templates/crystal"
require "../templates/deno"
require "../templates/elixir"
require "../templates/fish"
require "../templates/go"
require "../templates/kotlin"
require "../templates/node"
require "../templates/ruby"
require "../templates/rust"
require "../templates/scala"
require "../command/build"

class Generator
  getter config : Kamaji::Build::Config

  def initialize(@config)
  end

  def build
    custom_config = Configuration.read_config(config)
    gen_lang_files(custom_config.lang, custom_config)
    Kamaji::Project::Init.new(custom_config).run
  end

  def gen_lang_files(lang, config)
    case
    when lang == "assemblyscript" then Kamaji::AssemblyScript::Init.new(config).run
    when lang == "bash"           then Kamaji::Bash::Init.new(config).run
    when lang == "crystal"        then Kamaji::Crystal::Init.new(config).run
    when lang == "deno"           then Kamaji::Deno::Init.new(config).run
    when lang == "elixir"         then Kamaji::Elixir::Init.new(config).run
    when lang == "fish"           then Kamaji::Fish::Init.new(config).run
    when lang == "go"             then Kamaji::Go::Init.new(config).run
    when lang == "kotlin"         then Kamaji::Kotlin::Init.new(config).run
    when lang == "node"           then Kamaji::Node::Init.new(config).run
    when lang == "ruby"           then Kamaji::Ruby::Init.new(config).run
    when lang == "rust"           then Kamaji::Rust::Init.new(config).run
    when lang == "scala"          then Kamaji::Scala::Init.new(config).run
    else
    end
  end
end
