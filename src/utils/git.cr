module Kamaji::Git
  class_property executable = "git"

  def self.git_command(args, output : Process::Stdio = Process::Redirect::Close)
    status = Process.run(executable, args, output: output)
    status.success?
  rescue IO::Error
    false
  end

  def self.git_capture(args)
    String.build do |io|
      git_command(args, output: io) || return
    end
  end

  def self.git_config(key)
    git_capture(["config", "--get", key]).try(&.strip).presence
  end

  def self.init
    git_command(["init"], output: STDOUT)
  end

  def self.add_all
    git_command(["add", "."], output: STDOUT)
  end

  def self.initial_commit
    git_command(["commit", "-m", "'initial commit'"])
  end

  def self.rename_branch
    git_command(["branch", "-m", "master", "main"])
  end

  def self.status
    git_command(["status"], output: STDOUT)
  end

  def self.help
    git_command(["help"], output: STDOUT)
  end
end
