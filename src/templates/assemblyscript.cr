require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::AssemblyScript
  class Init < Project::Init
    def run
      if config.type == "app" || config.type == "wasm"
        [
          GitignoreView,
          AsConfig,
          BuildGitIgnoreView,
          IndexView,
          IndexTestView,
          IndexMainView,
          TsConfigView,
          Project::GitInitView,
        ].map { |template| Project::View.register(template) }
      else
        Messages.no_template_exists(config.type, config.lang)
        exit
      end
    end
  end

  template GitignoreView, "node.gitignore.ecr", ".gitignore", "#{__DIR__}/git"
  template BuildGitIgnoreView, "asbuild.gitignore.ecr", "build/.gitignore", "#{__DIR__}/git"

  template AsConfig, "asconfig.json.ecr", "asconfig.json", "#{__DIR__}/assemblyscript"
  template IndexView, "index.js.ecr", "index.js", "#{__DIR__}/assemblyscript"
  template IndexTestView, "index.test.js.ecr", "tests/index.js", "#{__DIR__}/assemblyscript"
  template IndexMainView, "index.ts.ecr", "assembly/index.ts", "#{__DIR__}/assemblyscript"
  template PackageJsonView, "package.json.ecr", "package.json", "#{__DIR__}/assemblyscript"
  template TsConfigView, "tsconfig.json.ecr", "assembly/tsconfig.json", "#{__DIR__}/assemblyscript"
end
