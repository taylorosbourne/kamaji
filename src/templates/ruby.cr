require "colorize"
require "./project"
require "../utils/macros"
require "../utils/messages"

module Kamaji::Ruby
  class Init < Project::Init
    def run
      case
      when config.type == "script" then [
        GitlabCiView,
        RubyScriptView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
      when config.type == "app" then [
        GitignoreView,
        GitlabCiView,
        RubyMainView,
        RubySpecView,
        RubyDockerfile,
        Gemfile,
        Rubocop,
        RubocopAirbnb,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
      else
        Messages.no_template_exists(config.type, config.lang)
        exit
      end
    end
  end

  template GitignoreView, "ruby.gitignore.ecr", ".gitignore", "#{__DIR__}/git"

  template RubyScriptView, "script.ecr", "bin/#{config.name}", "#{__DIR__}/ruby"
  template RubyMainView, "main.rb.ecr", "lib/main.rb", "#{__DIR__}/ruby"
  template RubySpecView, "spec_helper.ecr", "spec/spec_helper.rb", "#{__DIR__}/ruby"
  template RubyDockerfile, "dockerfile.ecr", "Dockerfile", "#{__DIR__}/ruby"
  template Gemfile, "gemfile.ecr", "Gemfile", "#{__DIR__}/ruby"
  template Rubocop, "rubocop-yml.ecr", ".rubocop.yml", "#{__DIR__}/ruby"
  template RubocopAirbnb, "rubocop_yml_airbnb.ecr", ".rubocop_airbnb.yml", "#{__DIR__}/ruby"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/ruby"
end
