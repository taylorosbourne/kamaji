require "colorize"
require "./project"
require "../utils/macros"
require "../utils/messages"

module Kamaji::Node
  class Init < Project::Init
    def run
      case
      when config.type == "script" then [
        GitlabCiView,
        NodeScriptView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
      when config.type == "app" then [
        GitignoreView,
        GitlabCiView,
        PackageJsonView,
        TsConfigView,
        IndexMainView,
        EslintRcView,
        PrettierRcView,
        PrettierIgnoreView,
        TypesDTsView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
      else
        Messages.no_template_exists(config.type, config.lang)
        exit
      end
    end
  end

  template GitignoreView, "node.gitignore.ecr", ".gitignore", "#{__DIR__}/git"

  template NodeScriptView, "script.ecr", "bin/#{config.name}", "#{__DIR__}/node"
  template PackageJsonView, "package.json.ecr", "package.json", "#{__DIR__}/node"
  template TsConfigView, "tsconfig.json.ecr", "tsconfig.json", "#{__DIR__}/node"
  template IndexMainView, "index.ts.ecr", "src/index.ts", "#{__DIR__}/node"
  template EslintRcView, "eslintrc.ecr", ".eslintrc", "#{__DIR__}/node"
  template PrettierRcView, "prettierrc.ecr", ".prettierrc", "#{__DIR__}/node"
  template PrettierIgnoreView, "prettierignore.ecr", ".prettierignore", "#{__DIR__}/node"
  template TypesDTsView, "types.d.ts.ecr", "src/types.d.ts", "#{__DIR__}/node"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/node"
end
