require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::Scala
  class Init < Project::Init
    def run
      [
        GitignoreView,
        GitlabCiView,
        ScalaMainView,
        PluginsSbtView,
        BuildPropView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
    end
  end

  template GitignoreView, "scala.gitignore.ecr", ".gitignore", "#{__DIR__}/git"

  template ScalaMainView, "myapp.scala.ecr", "src/main/scala/MyApp.scala", "#{__DIR__}/scala"
  template PluginsSbtView, "plugins.sbt.ecr", "project/plugins.sbt", "#{__DIR__}/scala"
  template BuildPropView, "build.properties.ecr", "project/build.properties", "#{__DIR__}/scala"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/scala"
end
