require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::Kotlin
  class Init < Project::Init
    def run
      [
        KotlinMainView,
        ResourcesGitkeepView,
        TestKotlinGitkeepView,
        TestResourcesGitkeepView,
        GradleBuildView,
        GradleSettingView,
        GradlePropView,
        GitignoreView,
        GitlabCiView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
    end
  end

  template GitignoreView, "kotlin.gitignore.ecr", ".gitignore", "#{__DIR__}/git"
  template ResourcesGitkeepView, "gitkeep.ecr", "src/main/resources/.gitkeep", "#{__DIR__}/git"
  template TestKotlinGitkeepView, "gitkeep.ecr", "src/test/kotlin/.gitkeep", "#{__DIR__}/git"
  template TestResourcesGitkeepView, "gitkeep.ecr", "src/test/resources/.gitkeep", "#{__DIR__}/git"

  template KotlinMainView, "main.kt.ecr", "src/main/kotlin/main.kt", "#{__DIR__}/kotlin"
  template GradleBuildView, "build.gradle.kts.ecr", "build.gradle.kts", "#{__DIR__}/kotlin"
  template GradleSettingView, "settings.gradle.kts.ecr", "settings.gradle.kts", "#{__DIR__}/kotlin"
  template GradlePropView, "gradle.properties.ecr", "gradle.properties", "#{__DIR__}/kotlin"
  template GitlabCiView, ".gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/kotlin"
end
