require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::Rust
  class Init < Project::Init
    def run
      [
        GitignoreView,
        GitlabCiView,
        RustMainView,
        CargoTomlView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
    end
  end

  template GitignoreView, "rust.gitignore.ecr", ".gitignore", "#{__DIR__}/git"

  template RustMainView, "main.rs.ecr", "src/main.rs", "#{__DIR__}/rust"
  template CargoTomlView, "cargo.toml.ecr", "Cargo.toml", "#{__DIR__}/rust"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/rust"
end
