require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::Go
  class Init < Project::Init
    def run
      [
        GitignoreView,
        GitlabCiView,
        GoMainView,
        GoDockerfile,
        GoMod,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
    end
  end

  template GitignoreView, "go.gitignore.ecr", ".gitignore", "#{__DIR__}/git"

  template GoMainView, "main.go.ecr", "main.go", "#{__DIR__}/go"
  template GoDockerfile, "dockerfile.ecr", "Dockerfile", "#{__DIR__}/go"
  template GoMod, "go.mod.ecr", "go.mod", "#{__DIR__}/go"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/go"
end
