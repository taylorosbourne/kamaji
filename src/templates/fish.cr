require "colorize"
require "./project"
require "../utils/macros"
require "../utils/messages"

module Kamaji::Fish
  class Init < Project::Init
    def run
      if config.type == "script"
        [
          GitlabCiView,
          FishMainView,
          Project::GitInitView,
        ].map { |template| Project::View.register(template) }
      else
        Messages.no_template_exists(config.type, config.lang)
        exit
      end
    end
  end

  template FishMainView, "main.fish.ecr", "bin/#{config.name}.fish", "#{__DIR__}/fish"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/bash"
end
