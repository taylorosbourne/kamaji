require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::Elixir
  class Init < Project::Init
    def run
      [
        GitignoreView,
        GitlabCiView,
        FormatterView,
        MixView,
        LibMainView,
        TestHelperView,
        TestMainView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
    end
  end

  template GitignoreView, "elixir.gitignore.ecr", ".gitignore", "#{__DIR__}/git"

  template FormatterView, "formatter.exs.ecr", "formatter.exs", "#{__DIR__}/elixir"
  template MixView, "mix.exs.ecr", "mix.exs", "#{__DIR__}/elixir"
  template LibMainView, "main.ex.ecr", "lib/#{config.name}.ex", "#{__DIR__}/elixir"
  template TestHelperView, "test_helper.exs.ecr", "test/test_helper.exs", "#{__DIR__}/elixir"
  template TestMainView, "main_test.exs.ecr", "test/#{config.name}_test.exs", "#{__DIR__}/elixir"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/elixir"
end
