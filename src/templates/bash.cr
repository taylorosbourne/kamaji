require "colorize"
require "./project"
require "../utils/macros"
require "../utils/messages"

module Kamaji::Bash
  class Init < Project::Init
    def run
      if config.type == "script"
        [
          GitlabCiView,
          BashMainView,
          Project::GitInitView,
        ].map { |template| Project::View.register(template) }
      else
        Messages.no_template_exists(config.type, config.lang)
        exit
      end
    end
  end

  template BashMainView, "main.sh.ecr", "bin/#{config.name}.sh", "#{__DIR__}/bash"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/bash"
end
