require "path"
require "../utils/macros"
require "../utils/git"
require "../command/build"

module Kamaji::Project
  class Init
    getter config : Build::Config

    def initialize(@config)
    end

    def run
      if Dir.exists?(config.expanded_dir)
        raise Build::Error.new("\n #{"Error:".colorize(:red)} your input: '#{config.dir.colorize(:yellow)}' already exists")
      end

      views = self.views
      views.each &.render
    end

    private def views
      View.views.map(&.new(config))
    end
  end

  abstract class View
    getter config : Build::Config
    getter full_path : ::Path

    @@views = [] of View.class

    def self.views
      @@views
    end

    def self.register(view)
      views << view
    end

    def initialize(@config)
      @full_path = config.expanded_dir.join(path)
    end

    def render
      Dir.mkdir_p(full_path.dirname)
      File.write(full_path, to_s)
      puts "    #{"create".colorize(:green)}  #{full_path}"
    end

    def module_name
      config.name.split('-').map(&.camelcase).join("::")
    end

    abstract def path
  end

  class GitInitView < View
    def render
      Git.git_command(["init", config.dir])
      puts "    #{"init".colorize(:green)}    an empty #{"git".colorize(:green)} repo in '#{config.dir.colorize(:white)}'"
      Dir.cd(config.dir)
      Git.add_all
      puts "    #{"add".colorize(:green)}     generated files"
      Git.initial_commit
      puts "    #{"commit".colorize(:green)}  generated files"
      Git.rename_branch
      puts "    #{"rename".colorize(:green)}  #{"master".colorize(:red)} branch to #{"main".colorize(:green)}"
    end

    def path
      ".git"
    end
  end

  template EditorconfigView, "editorconfig.ecr", ".editorconfig", "#{__DIR__}/project"
  template LicenseView, "license.ecr", "LICENSE", "#{__DIR__}/project"
  template ReadmeView, "readme.md.ecr", "README.md", "#{__DIR__}/project"

  [EditorconfigView, LicenseView, ReadmeView].map { |template| View.register(template) }
end
