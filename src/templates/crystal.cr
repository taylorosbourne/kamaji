require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::Crystal
  class Init < Project::Init
    def run
      [
        Dockerfile,
        GitignoreView,
        GitlabCiView,
        ShardView,
        SrcMainView,
        SpecHelperView,
        SpecExampleView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
    end
  end

  template GitignoreView, "crystal.gitignore.ecr", ".gitignore", "#{__DIR__}/git"

  template Dockerfile, "dockerfile.ecr", "Dockerfile", "#{__DIR__}/crystal"
  template ShardView, "shard.yml.ecr", "shard.yml", "#{__DIR__}/crystal"
  template SrcMainView, "main.cr.ecr", "src/#{config.name}.cr", "#{__DIR__}/crystal"
  template SpecHelperView, "spec_helper.cr.ecr", "spec/spec_helper.cr", "#{__DIR__}/crystal"
  template SpecExampleView, "main_spec.cr.ecr", "spec/#{config.name}_spec.cr", "#{__DIR__}/crystal"
  template GitlabCiView, "gitlab-ci.yml.ecr", ".gitlab-ci.yml", "#{__DIR__}/crystal"
end
