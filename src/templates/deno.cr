require "colorize"
require "./project"
require "../utils/macros"

module Kamaji::Deno
  class Init < Project::Init
    def run
      [
        DenoMainView,
        DenoTypesView,
        Project::GitInitView,
      ].map { |template| Project::View.register(template) }
    end
  end

  template DenoMainView, "mod.ts.ecr", "mod.ts", "#{__DIR__}/deno"
  template DenoTypesView, "types.d.ts.ecr", "types.d.ts", "#{__DIR__}/deno"
end
