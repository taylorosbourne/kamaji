# Kamaji CLI

### What? 🛠️
- Kamaji is the boilerman!  He generates boilerplate code!  In other words, Kamaji is a CLI program for quickly initiating new programming projects

### Why? 🤔
- Productivity!  Get started quickly and never write the same code twice
- Kamaji was initially developed as a way to learn the Crystal programming language

### How? ❓
- Its easy!  You can output Kamaji's help menu for usage reminders by running ```kamaji -h```, ```kamaji --help```, or simply ```kamaji```
- Initiating a new project is as easy as knowing what you would like to name your project and what language you would like to work with; use the ```build``` command to create a new project:
```kamaji build LANG TYPE (NAME)```
- Ex. ```kamaji build elixir app my_elixir_app```
- Usage conituned below

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Supported Languages/Runtimes](#supported-languages/runtimes)
- [Credits & inspirations](#credits-&-inspirations)
- [Contributors](#contributors)

## Install

Install from source

```shell
  git clone https://gitlab.com/taylorosbourne/kamaji.git
  cd kamaji
  crystal build src/kamaji.cr -o kamaji -p --release --no-debug
  chmod +x ./kamaji # make executbale
  mv ./kamaji ~/.crystal/bin/kamaji # move executable
  export PATH=\"$HOME/.crystal/bin:$PATH\" # update path
```

## Usage

```
Usage: kamaji build LANG TYPE NAME

  Initializes a project folder as a git repository and 
  default folder structure for programming projects.

    -h, --help                       show this help

  LANG Options
    - bash
    - crystal
    - deno
    - elixir
    - fish
    - go
    - node
    - ruby
    - rust
    - scala
  
  TYPE Options
    - app
    - cli
    - lib
    - script
    - wasm
  
  Note:
    Not all TYPE options are valid with all LANG options.  
    Read the about valid options here https://gitlab.com/taylorosbourne/kamaji
```

Ex. ```kamaji build elixir app my_elixir_app```

## Supported Languages/Runtimes

### AssemblyScript 🚀 [Docs](https://www.assemblyscript.org/introduction.html) | [Source](https://github.com/AssemblyScript/assemblyscript)
AssemblyScript compiles a strict variant of TypeScript(a typed superset of JavaScript) to WebAssembly using Binaryen.  Its architecture differs from a JavaScript VM in that it compiles a program ahead of time, quite similar to your typical C compiler. One can think of it as TypeScript syntax on top of WebAssembly instructions, statically compiled, to produce lean and mean WebAssembly binaries.

Command example:
```kamaji build assemblyscript app my_assemblyscript_app```

Built project:
```
my_assemblyscript_app
├── assembly
│   └── index.ts
│   └── tsconfig.json
├── build
│   └── .gitignore
├── tests
│   └── index.js
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── asconfig.json
├── index.js
├── package.json
├── LICENSE
└── README.md
```

### Bash 💲 [Docs](https://devdocs.io/bash/) | [Source](https://github.com/bminor/bash)
Bash is a Unix shell and command language written by Brian Fox for the GNU Project as a free software replacement for the Bourne shell. First released in 1989, it has been used as the default login shell for most Linux distributions.

Command example:
```kamaji build bash script my_bash_script```

Built project:
```
my_bash_script
├── bin
│   └── my_bash_script.sh
├── .editorconfig
├── .gitlab-ci.yml
├── LICENSE
└── README.md
```

### Crystal 🔮 [Docs](https://crystal-lang.org/docs/) | [Source](https://github.com/crystal-lang/crystal)
Crystal is a statically type-checked programming language with syntax similar to Ruby.  Kamaji is written in Crystal, and makes heavy use of code in crystal's ```init``` command for codegen.  Initializing a Crystal project from Kamaji will essentially produce the same files as ```crystal init app``` with the addition of ```Dockerfile```, ```.gitlab-ci.yml```, and more.

Command example:
```kamaji build crystal app my_crystal_app```

Built project:
```
my_crystal_app
├── spec
│   ├── my_crystal_app_spec.cr
│   └── spec_helper.cr
├── src
│   └── my_crystal_app.cr
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
├── README.md
└── shard.yml
```

### Deno 🦕 [Docs](https://deno.land/manual) | [Source](https://github.com/denoland/deno)
Deno is a secure runtime for JavaScript and TypeScript.  Deno is written in Rust and actively maintained/developed by Ryan Dahl, creator of Nodejs.  Although both Node, and Deno are JS runtimes, they vary greatly.  Check the Deno documentation/style guide for information on how/why Deno programs do not follow the same conventions as Node programs.

Command example:
```kamaji build deno app my_deno_app```

Built project:
```
my_deno_app
├── .editorconfig
├── LICENSE
├── mod.ts
├── README.md
└── types.d.ts
```

### Elixir 🧪 [Docs](https://elixir-lang.org/docs.html) | [Source](https://github.com/elixir-lang/elixir)
Elixir is a dynamic, functional programming language.  Elixir leverages the Erlang VM, known for running low-latency, distributed, and fault-tolerant systems. Elixir is successfully used in web development, embedded software, data ingestion, and multimedia processing, across a wide range of industries. 

Command example:
```kamaji build elixir app my_elixir_app```

Built project:
```
my_elixir_app
├── lib
│   └── my_elixir_app.ex
├── test
│   ├── my_elixir_app_test.exs
│   └── test_helpers.exs
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── formatter.exs
├── LICENSE
├── mix.exs
└── README.md
```

### Fish 🐠 [Docs](https://fishshell.com/docs/current/index.html) | [Source](https://github.com/fish-shell/fish-shell)
Fish (or Friendly Interactive Shell) is a smart and user-friendly command line shell for Linux, macOS, and the rest of the family.  Fish suggests commands as you type based on history and completions, just like a web browser. Watch out, Netscape Navigator 4.0!

Command example:
```kamaji build fish script my_fish_script```

Built project:
```
my_fish_script
├── bin
│   └── my_fish_script.fish
├── .editorconfig
├── .gitlab-ci.yml
├── LICENSE
├── README.md
```

### Go 🐹 [Docs](https://golang.org/doc/) | [Source](https://github.com/golang/go)
Go is an open source, compiled programming language developed by a team at Google and many [contributors](https://golang.org/CONTRIBUTORS) from the open source community.  Go powers industry-changing technologies like [Docker](https://www.docker.com/) and [Kubernetes](https://kubernetes.io/).  Go is distributed under a BSD-style license.

Command example:
```kamaji build go app my_go_app```

Built project:
```
my_go_app
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── go.mod
├── LICENSE
├── main.go
└── README.md
```

### Node 🔌 [Docs](https://nodejs.org/en/docs/) | [Source](https://github.com/nodejs/node)
Node is a JavaScript runtime built on [Chrome's V8 JavaScript engine](https://v8.dev/).  Node has an enormous community that is, in many ways, shaping the way modern web applications are built.  Due to JavaScript's outrageous popularity, many dependable libraries are built for use with Node. 

Command example:
```kamaji build node app my_node_app```

Built project:
```
my_node_app
├── src
│   ├── index.ts
│   └── types.d.ts
├── .editorconfig
├── .eslintrc
├── .gitignore
├── .gitlab-ci.yml
├── .prettierignore
├── .prettierrc
├── LICENSE
├── package.json
├── README.md
└── tsconfig.json
```

### Ruby 💎 [Docs](https://www.ruby-lang.org/en/documentation/) | [Source](https://github.com/ruby/ruby)
Ruby is a dynamic, open source programming language with a focus on simplicity and productivity. It has an elegant syntax that is natural to read and easy to write.  Ruby creates a developer experience unlike any other (except maybe Crystal...), a language truly worthy of it's tagline: _A PROGRAMMER'S BEST FRIEND_.

Command example:
```kamaji build ruby app my_ruby_app```

Built project:
```
my_ruby_app
├── spec
│   └── spec_helper.rb
├── src
│   └── main.rb
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── .rubocop_airbnb.yml
├── .rubocop.yml
├── Dockerfile
├── Gemfile
├── LICENSE
└── README.md
```

### Rust 🦀 [Docs](https://doc.rust-lang.org/std/index.html) | [Source](https://github.com/rust-lang/rust)
Rust is fast and memory-efficient: with no runtime or garbage collector, it can power performance-critical services, run on embedded devices, and easily integrate with other languages.  Rust’s type system and ownership model guarantee memory-safety and thread-safety — enabling you to eliminate many classes of bugs at compile-time.  Rust placed at the top of StackOverflow's most loved programming language of 2020.  In fact, for five years running, Rust has taken the top spot as the most loved programming language.

Command example:
```kamaji build rust app my_rust_app```

Built project:
```
my_rust_app
├── src
│   └── main.rs
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── Cargo.toml
├── LICENSE
└── README.md
```

### Scala ♨️ [Docs](https://docs.scala-lang.org/) | [Source](https://github.com/scala/scala) 
Scala combines object-oriented and functional programming in one high-level language. Scala's static types help avoid bugs in complex applications, and its JVM and JavaScript runtimes let you build high-performance systems with easy access to huge ecosystems of libraries.

Command example:
```kamaji build scala app my_scala_app```

Built project:
```
my_scala_app
├── project
│   ├── build.properties
│   └── plugins.sbt
├── src
│   └── main
│       └── scala
│           └── MyApp.scala
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── LICENSE
└── README.md
```

## Credits & inspirations

- [crystal init](https://crystal-lang.org/reference/using_the_compiler/index.html#crystal-init)
- [mix new](https://elixir-lang.org/getting-started/mix-otp/introduction-to-mix.html)
- [cargo new](https://doc.rust-lang.org/cargo/getting-started/first-steps.html)

## Contributors

- Taylor Osbourne, [Gitlab](https://gitlab.com/taylorosbourne), [Website](https://taylorosbourne.com) - creator, maintainer