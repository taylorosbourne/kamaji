FROM crystallang/crystal:0.19.2

# Install crystal deps
COPY shard.yml /app/
COPY shard.lock /app/
WORKDIR /app
RUN crystal deps

# Add app and build it
COPY . /app
RUN crystal build src/kamaji.cr -o kamaji -p --release --no-debug

# Run it!
CMD "./kamaji"